
import com.thunwarat.databaseproject.dao.OrdersDao;
import com.thunwarat.databaseproject.model.OrderDetail;
import com.thunwarat.databaseproject.model.Orders;
import com.thunwarat.databaseproject.model.Product;

public class TestOrder {
    public static void main(String[] args) {
        Product product1 = new Product(1, "A", 75);
        Product product2 = new Product(2, "B", 180);
        Product product3 = new Product(3, "C", 70);
        Orders order = new Orders();
        order.addOrderDetail(product1,1);
        order.addOrderDetail(product2,1);
        order.addOrderDetail(product3,1);
        System.out.println(order);
        System.out.println(order.getOrderDetails());
        printReciept(order);
        
        OrdersDao ordersDao = new OrdersDao();
        Orders newOrders = ordersDao.save(order);
        System.out.println(newOrders);
         Orders order1 = ordersDao.get(8);
        printReciept(order1);

    }
    
    static void printReciept(Orders order){
        System.out.println("Order  " + order.getId());
        for(OrderDetail od: order.getOrderDetails()){
            System.out.println(" " + od.getProductName() + " " + od.getQty() + " " + od.getProductPrice() + " " + od.getTotal());
            System.out.println("Total: " + order.getTotal() + " Qty: " + order.getQty());
        }
    }
}
