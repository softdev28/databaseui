/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.databaseproject.dao;

import com.thunwarat.databaseproject.Helper.DatabaseHelper;
import com.thunwarat.databaseproject.model.OrderDetail;
import com.thunwarat.databaseproject.model.Orders;
import com.thunwarat.databaseproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class OrderDetailDao implements Dao<OrderDetail> {

    @Override
    public OrderDetail get(int id) {
        OrderDetail item = null;
        String sql = "SELECT * FROM order_detail WHERE order_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = OrderDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    

    @Override
    public List<OrderDetail> getAll() {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<OrderDetail> getAll(String where, String order) {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
   public List<OrderDetail> getByOrdersId(int orderId) {
        return getAll("order_id = " + orderId, " order_detail_id ASC ");
    }
    

    public List<OrderDetail> getAll(String order) {
        ArrayList<OrderDetail> list = new ArrayList();
        String sql = "SELECT * FROM order_detail  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                OrderDetail item = OrderDetail.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public OrderDetail save(OrderDetail obj) {

        String sql = "INSERT INTO order_detail (product_id, qty, product_price, product_name, order_id)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            //stmt.setInt(1, obj.getProduct().getId());
            stmt.setInt(2, obj.getQty());
            stmt.setDouble(3, obj.getProductPrice());
            stmt.setString(4, obj.getProductName());
            stmt.setInt(5, obj.getOrder().getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public OrderDetail update(OrderDetail obj) {
    return null;
    }

    @Override
    public int delete(OrderDetail obj) {
        String sql = "DELETE FROM order_detail WHERE order_detail_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}


