/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.databaseproject.dao;

import java.util.List;

/**
 *
 * @author ACER
 */
public interface Dao<T> {
    T get(int id);
    List<T> getAll();
    T save (T obj);
    T update (T obj);
    int delete(T obj);
    List<T> getAll(String where, String order);
}
