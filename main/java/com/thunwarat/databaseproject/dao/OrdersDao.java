/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.databaseproject.dao;

import com.thunwarat.databaseproject.Helper.DatabaseHelper;
import com.thunwarat.databaseproject.model.OrderDetail;
import com.thunwarat.databaseproject.model.Orders;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ACER
 */
public class OrdersDao implements Dao<Orders> {

    @Override
    public Orders get(int id) {
        Orders order = null;
        String sql = "SELECT * FROM orders WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                order = Orders.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return order;
    }

    @Override
    public List<Orders> getAll() {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Orders> getAll(String where, String order) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Orders> getAll(String order) {
        ArrayList<Orders> list = new ArrayList();
        String sql = "SELECT * FROM orders  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Orders item = Orders.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Orders save(Orders obj) {
        OrderDetailDao orderDetailDao = new OrderDetailDao();
        String sql = "INSERT INTO orders (order_total, order_qty, order_date)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setInt(2, obj.getQty());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            stmt.setString(3, sdf.format(obj.getDate()));
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
           // obj = get(id);
            obj.setId(id);
            for(OrderDetail od: obj.getOrderDetails()){
                orderDetailDao.save(od);
            }
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        //return obj;
    }

    @Override
    public Orders update(Orders obj) {
        return null;
    }

    @Override
    public int delete(Orders obj) {
        String sql = "DELETE FROM orders WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

}
