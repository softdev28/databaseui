/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thunwarat.databaseproject2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ACER
 */
public class DeleteDatabase {

    public static void main(String[] arge) {
        Connection conn = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Insert
        //String sql = "INSERT INTO category(category_id,category_name) VALUES (?, ?)";
        String sql = "DELETE  FROM category WHERE category_id = ? ";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            //stmt.setInt(1, 3);
            stmt.setInt(1, 4);
            //stmt.setInt(2, 1);
            int status = stmt.executeUpdate();
            //ResultSet key = stmt.getGeneratedKeys();
            //key.next();
            // System.out.println(" " + key.getInt(1));
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //CloseDatabase 
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
